__author__ = 'fstrub'

#This script enable to reduce the size of a jester file
# jester format :
# for eah user :
# noRating rate1 rate2 rate3 ...
# rateX = 99 if missing

NO_FEATURES = 100
SEPARATOR = ','

import os

workdir = "/home/fstrub/Projects/NNMatrixFactorisation/data/"

inputFilePath = os.path.join(workdir, "jester/jester-data-1.csv")
outPutFilePath = os.path.join(workdir, "jester/jester-data-1-dense-150*100.csv")

noUsers = 0
storedUsers = 0

onlyDenseUsers = True
removeFirstColumn = True
maxNoUser = 150


with open(inputFilePath, 'r') as inputFile:
    with open(outPutFilePath, 'w') as outputFile:

            #only copy lines that does not incldue 99
            for line in inputFile:

                #decompose the line with noRates/rates..
                noRates, _, rates = line.partition(SEPARATOR)

                if removeFirstColumn:
                    line = rates

                if onlyDenseUsers:
                    if int(noRates) == NO_FEATURES:
                        outputFile.write(line)
                        storedUsers += 1
                else:
                    outputFile.write(line)
                    storedUsers += 1

                noUsers += 1

                #trunc users
                if 0 < maxNoUser <= storedUsers:
                    break




print ('{0} of dense users over {1} were extracted'.format(storedUsers, noUsers))
