require ("torch")
require ("dok")

dofile ("tools.lua")


--- Load a a N*M matrix to pre-process.
-- One need to set pre-processing options
-- @param data torch.Tensor, Tensor of size N*M', require
-- @param normalize boolean, center data from zero to one, default=true
-- @param binary boolean, use either 0 or 1, default=true
-- @return pre-processed data set
-- 
local cache = {}

function preprocess(data, config)

   local rescale = config.preProcessing.rescale
   local binary    = config.preProcessing.binary   
   local apply     = config.preProcessing.apply   


   -- defensive copy
   local data = data:clone()


   --remember the NaN position
   local NaNMask = data:eq(NaN)


   if rescale then
      local data_no_NaN = data[data:ne(NaN)]

      --ignore NaN to normalize 
      local min = math.min(data_no_NaN:min(), 0)
      local max = data_no_NaN:max()
      
      local newMin = rescale[1]
      local newMax = rescale[2]

      data:add(-min) --> positive values
      data:div(max-min) --> normalize over 1

      data:mul(newMax-newMin) --> new norm 
      data:add(newMin) --> center again

      cache.min = min
      cache.max = max
      
      cache.newMin = newMin
      cache.newMax = newMax
      

   end

   if binary then
      --ignore NaN to normalize 
      data = data:gt(binary):double()
   end


   if apply then
      data:apply(apply)
   end

   -- reset NaN if they were normalized
   data[NaNMask] = NaN

   return data

end


function postprocess(data, config, hasNaN)

   if hasNaN == nil then hasNaN = true end

   if config.postProcessing then

      local apply   = config.postProcessing.apply
      local rescale = config.postProcessing.rescale
   
      local NaNMask = nil
      if hasNaN == true then
         NaNMask = data:eq(NaN)
      end
   
      -- first apply transformation
      if apply then
         data:apply(apply)
      end
   
      -- rescale to original size 
      if rescale == true then
      
         local min = cache.min
         local max = cache.max
         
         local newMin = cache.newMin
         local newMax = cache.newMax
         
         data:add(-newMin) --> positive values
         data:div(newMax-newMin) --> normalize over 1

         data:mul(max-min) --> new norm 
         data:add(min) --> center again
         
      end
   
   
      -- reset NaN if they were normalized
      if NaNMask then
         data[NaNMask] = NaN
      end

   end
   
   return data
end




--- Randomly generate a training/testing data set.
-- @param : training ratio (0 < trainingRatio < 1)
-- @return : training set, testing set
function GenerateTrainAndTestMask(data, trainingRatio)

   log("ratioTraining \t : " .. trainingRatio)

   assert(trainingRatio < 1 and trainingRatio > 0)

   -- compute the mask according the training ratio (NaN has no impact since the distribution is uniform) 
   local maskTrain = torch.Tensor(data:nElement()):uniform():gt(trainingRatio) 
   local maskTest  = maskTrain:eq(0)

   function applyMask(mask)
      return function(data)
         local data = data:clone()
         data[mask] = NaN
         return data
      end
   end

   return applyMask(maskTrain), applyMask(maskTest)

end


