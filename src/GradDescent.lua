require("nn")
require("torch")

dofile ("tools.lua")


SigGrad = {}




------------------------

LogisticBCE = { name = "LogisticBCEGrad" }
function LogisticBCE:tensor(U,V)
   return Sigmoid(U*V:t())
end
function LogisticBCE:compute(u,v, t)
   return Sigmoid(u:dot(v)) - t 
end

-------------------------

LogisticMSE = { name = "LogisticMSEGrad" }
function LogisticMSE:tensor(U,V)
   return Sigmoid(U*V:t())
end
function LogisticMSE:compute(u,v, t)
   local s = Sigmoid(u:dot(v))
   return -2*(t-s)*s*(1-s)
end

-------------------------

LinearMSE = { name = "LinearGrad"  }
function LinearMSE:tensor(U,V)
   return U*V:t()
end
function LinearMSE:compute(u,v, t)
   return u:dot(v)-t
end




GradDescent = {} 



function GradDescent:new(errFct)
   newObj = 
      {
         name = errFct.name,
         U = torch.Tensor(),
         V = torch.Tensor(),
         errFct = errFct
      }

   self.__index = self                      
   return setmetatable(newObj, self)        
end


function GradDescent:init(U,V, train, conf)

   -- defensive copy
   self.U = U:clone()
   self.V = V:clone()

   --store conf
   self.lambda = conf.lambda
   self.rank   = conf.rank
   self.lrt    = conf.lrt

   --precompute data
   self.targets = computeSpareVector(train)
end


function GradDescent:eval(M)

   -- pre-allocate memory
   if self.du == nil then self.du = torch.zeros(self.U[1]:size()) end
   if self.dv == nil then self.dv = torch.zeros(self.V[1]:size()) end


   local targets = self.targets 

   local shuffle = torch.randperm(#targets)

   -- main algo
   for k = 1, shuffle:size(1) do
      local target = targets[k]

      local i = target.i
      local j = target.j
      local t = target.val

      local u = self.U[i]
      local v = self.V[j]

      local err = self.errFct:compute(u,v,t)

      self.du:copy(v):mul(err):add(self.lambda, u)
      self.dv:copy(u):mul(err):add(self.lambda, v)

      u:add(-self.lrt, self.du)
      v:add(-self.lrt, self.dv)

   end

   M:copy(self.errFct:tensor(self.U,self.V))
   return M

end


