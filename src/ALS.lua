require("nn")
require("torch")

dofile ("tools.lua")
dofile ("AlgoTools.lua")

-- ALS, implementing 
-- http://www.grappa.univ-lille3.fr/~mary/cours/stats/centrale/reco/paper/MatrixFactorizationALS.pdf


ALS = {} 

function ALS:new(conf)
  newObj = 
  {
     name = "ALS",
     U = torch.Tensor(),
     V = torch.Tensor(),
  }
  
  self.__index = self
  
  if conf.useBias == true then
      newObj.bias = {}
      newObj.name = newObj.name .. "-bias"
  end
                  
  return setmetatable(newObj, self)        
end

function ALS:init(U,V, train, conf)

   -- defensive copy
   self.U = U:clone()
   self.V = V:clone()

   --store conf
   self.lambda = conf.lambda
   self.rank   = conf.rank
   
   --precompute data
   self.sparseRates = {}

   self.sparseRates[torch.pointer(self.U)] = computeSpareRates(train)
   self.sparseRates[torch.pointer(self.V)] = computeSpareRates(train:t())
   
   if self.bias then
      self.bias[torch.pointer(self.U)] = computeBias(train)
      self.bias[torch.pointer(self.V)] = computeBias(train:t()) 
   end

   
   

end



function ALS:eval(M)


   --- Solve the ALS linear system:
   --
   -- sum( x'y - r)*y_k + Reg = 0
   --
   -- @param X (in/out) vector to update
   -- @param data (in) input data
   -- @param Y (in) vector that is set
   local function updateVector(X, Y)

      --retrieve the sparse data of the fix vector 
      local sparseRate = self.sparseRates[torch.pointer(X)]
       
       
      for i = 1, X:size(1) do

         local data = sparseRate[i]

         -- retrieve the sparsed rated items/users
         local r_          = data.rates
         local noRatedElem = data.noRatedElem
         local mask        = data.mask

         if data.noRatedElem > 0 then

            -- Retrieve the rated user/item columns 
            local Y_ = Y[mask:expandAs(Y)]:view(noRatedElem, self.rank):t() 

            -- compute the regularization
            local Regu = torch.Tensor(self.rank):fill(self.lambda*noRatedElem):diag()

            -- step 2 : Solve the linear system
            local A = Y_*Y_:t() + Regu
            local b = Y_*r_


            if self.useBias then
               local bias = self.bias[torch.pointer(Y)][mask] -- Ybias
               bias:add(self.bias[torch.pointer(X)][i])       -- Xbias
               b:addmv(-1, Y_, bias)                          -- b = b - Y_*(Xbias[i] + Ybias[:])
            end

            -- solve linear system
            X[i]:copy(torch.gesv(b:view(-1,1),A))

         end
      end
   end

   -- Main algorithm
   updateVector(self.U, self.V)
   updateVector(self.V, self.U)
       
   M:copy(self.U*self.V:t())
   return M
   
end




