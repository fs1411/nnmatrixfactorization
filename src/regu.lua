-- l2 regularization module
-- lambda: the regularization parameter
function reguL2(lambda)

   local regularizer = {}

   -- The loss value of this regularizer
   function regularizer:forward(input)
       return torch.sum(torch.pow(input,2))*lambda
   end

   -- The gradient of l with respect to w
   function regularizer:backward(input)
      self.dr = self.dr or torch.Tensor()
      self.dr:resizeAs(input):copy(input)
      self.dr:mul(2*lambda)
      return self.dr
   end

   -- The 2nd derivative of l with respect to w
   function regularizer:backward2(input)

       if self.dr2 ~= nil then
           self.dr2 = torch.Tensor()
           self.dr2:resizeAs(input):fill(2*lambda)
       end

       return self.dr2
   end

   -- Return this regularizer
   return regularizer
end


-- l2 regularization module
-- lambda: the regularization parameter
function reguTikhonov(lambda)

   local regularizer = {}

   -- The loss value of this regularizer
   function regularizer:forward(input, noRatedElem)
       return torch.sum(torch.pow(input,2))*lambda
   end

   -- The gradient of l with respect to w
   function regularizer:backward(input, noRatedElem)
      self.dr = self.dr or torch.Tensor()
      self.dr:resizeAs(input):copy(input)
      self.dr:mul(2*noRatedElem*lambda)
      return self.dr
   end

   -- The 2nd derivative of l with respect to w
   function regularizer:backward2(input, noRatedElem)

       if self.dr2 ~= nil then
           self.dr2 = torch.Tensor()
           self.dr2:resizeAs(input):fill(2*noRatedElem*lambda)
       end

       return self.dr2
   end

   -- Return this regularizer
   return regularizer
end