-- Load global libraries
require("nn")
require("optim")
require("sys") 
require("xlua") 

dofile ("tools.lua")


dofile ("iniFactory.lua")
dofile ("dataLoader.lua")
dofile ("dataPreprocessing.lua")
dofile ("dataPloter.lua")

dofile ("Algo.lua")

dofile ("AlternateNN.lua")
dofile ("ALS.lua")
dofile ("IRLS.lua")
dofile ("GradDescent.lua")
dofile ("loss.lua")

dofile ("dataPloter.lua")

dofile("logger.lua")


local root = '..'


--local dataType = "movieLens"
--local ratesFile  = root .. '/data/movieLens/ratings-1M.dat'
--local moviesFile = root .. '/data/movieLens/movies-1M.dat'
--local usersFile  = root .. '/data/movieLens/users-1M.dat'


local dataType = "jester"
local ratesFile = root .. '/data/jester/jester-data-1-dense-full.t7'
--local ratesFile = root .. '/data/jester/jester-data-1-dense-150*100.t7'
--local ratesFile = root .. '/data/jester/jester-data-1-sparse-150*100.t7'
--local ratesFile = root .. '/data/jester/jester-data-1-sparse-full.t7'

local outputDir = root .. '/output/'.. dataType 
local logFile   = root .. '/output/'.. dataType .. '/logs.txt'


local trainingRatio = 0.40
local Krank = 10
local epoches = 20


local algoList = {
   {
      algo = ALS:new({useBias = true}),
      lambda = 0.02,
   },
--   {
--      algo   = AlternateNN:new({ useSigmoid = true}),
--      lambda = 0.005,
--      lrt    = "Hessian",
--      preProcessing =      
--      {
--         rescale = {-0.5,0.5},
--      }
--   },
   {
      algo   = AlternateNN:new(),
      lambda = 0.005,
      lrt    = "Hessian",
      preProcessing =      
      {
         rescale = {-0.5,0.5},
      }
   },
--   {
--      algo = ALS:new({useBias = false}),
--      lambda = 0.02,
--
--      preProcessing =    
--      {
--         --apply = function(x) return Logit(x) end 
--      },
--
--      postProcessing =    
--      {
--         --apply = function(x) return Sigmoid(x) end 
--      }
--   },
--   {
--      algo = IRLS:new(),
--      lambda = 0.01,
--   },
--   {
--      algo   = GradDescent:new(LinearMSE),
--      lambda = 0.02,
--      lrt    = 0.01,
--   },
--   {
--      algo   = GradDescent:new(LogisticMSE),
--      lambda = 0.01,
--      lrt    = 0.02,
--   },
--   {
--      algo   = GradDescent:new(LogisticBCE),
--      lambda = 0.01,
--      lrt    = 0.02,
--   }
}

local defaultAlgoConf =  
   {
      rank = Krank,

      lossToOptim = "MSE",
      earlyStopping = false,

      initialization =    
      {
         meanUser = {},
         meanItem = {},
        --meanUserUnbiased = {},
        --meanItemUnbiased = {},
         rootSquare = {},
      },

      preProcessing =      
      {
         rescale = {0,1},
      -- apply = function(x) return x end 
      -- whitening -> todo?
      -- binary = 0.8
      },

      postProcessing =    
      {
       rescale = true,
        -- apply = function(x) return x end 
      },

      displayAdvance = true,
      plot = true
   }



------------------
--
--    FRAMEWORK PREPATION 
--
--

-- Prepare a new directory to store the results and backup previous files
rollFile(outputDir)
os.execute("mkdir " .. outputDir)

--configure logger
logger = logging.dual(logFile)

-- shortcut for logging
function log(m) logger:info(m) end


-- prepare plotter
printer = Printer:new(defaultAlgoConf, outputDir, dataType)



-- merge configuration from defaultAlgoConf into single algo conf

for _, algo in pairs(algoList) do
   algo = merge(algo, defaultAlgoConf)
   algo.name = algo.algo.name

   algo.fileName = algo.algo.name  --.. "-" ..
      --"lambda_" .. algo.lambda .. "-" ..
      --"lrt_ "   .. algo.lrt    .. "-" ..
      --"rank_"  .. algo.rank
end



------------------
--
--    LOAD DATA
--
--


local rawData = LoadData(
   {  
      name   = dataType,
      rates  = ratesFile,
      movies = moviesFile,
      users  = usersFile
   })




------------------
--
--    GENERATE DATA SAMPLE
--
--

local maskTrainFct, maskTestFct = GenerateTrainAndTestMask(rawData, trainingRatio)


------------------
--
--    INITIALIZATION
--




log("--- INITIALIZATION ---")
log("")

if defaultAlgoConf.plot then
   gnuplot.figure(1000)
   gnuplot.title("raw data")
   gnuplot.hist(rawData[rawData:ne(NaN)])
end



log("algo : ")
for _, algo in pairs(algoList) do
   log("\t - " .. algo.algo.name)
end

print("")



------------------
--
--    COMPUTE
-- 



for i, algo in pairs(algoList) do

   log("--- " .. algo.name .. " ---")
   log("")
   if algo.lambda then log("lambda \t : " .. algo.lambda) end
   if algo.lrt    then log("lrt    \t : " .. algo.lrt) end
   if algo.rank   then log("rank   \t : " .. algo.rank) end
   log("")  


   --pre-process data
   local data = preprocess(rawData, algo)

   local train = maskTrainFct(data)
   local test  = maskTestFct(data)


   --initialize data for algorithm
   local factory = IniFactory:new(train, algo)
   local U, V = factory:Build(algo.initialization)
   local M_best = U*V:t()
   local M_last
   
   
   -- start saving loss
   local loss = lossStorage:new(epoches, train, test, algo)
   loss:computeLoss(M_best, algo)



   log("Init:")
   printer:plotRatesDistribution(M_best, algo.fileName .. ".ini", train, test)
   printer:loss(loss)


   -- Execute algorithm
   local M_best, M_last = executeAlgo(U, V, loss, train, test, epoches, algo)
   algo.loss = loss:build()


   log("Final:")
   printer:plotRatesDistribution(M_last , algo.fileName .. ".overfit", train, test)
   printer:plotRatesDistribution(M_best , algo.fileName .. ".best", train, test)
   printer:loss(loss)
   
   printer:storeResults(M_best, algo.fileName)
   
   -- force garbage collector with high
   data = nil
   loss = nil
   algo.algo = nil
   M_best = nil
   M_last = nil 
   collectgarbage("collect")
      
end


printer:plotLoss(algoList)


log("")
log("Done !!! :-)")
log("")




