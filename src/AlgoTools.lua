require("nn")
require("torch")

dofile ("tools.lua")


--- This method compute a sparse rates Matrix (with no NaN)
function computeSpareRates(train)
          
      local res = {}

      for i = 1, train:size(1) do
         local mask = train[i]:ne(NaN):view(-1,1) -- trick to use a 2D vector
         local noRatedElem = mask:sum()
      
         res[i] = {}
         
         res[i].mask        = mask
         res[i].noRatedElem = noRatedElem
         res[i].rates       = train[i][mask]
         
      end
      
      return res         
end

function computeBias(train)
      
      local mean = train[train:ne(NaN)]:mean()
      
      local bias = torch.Tensor(train:size(1))
      for i = 1, train:size(1) do
         local t = train[i] 
         bias[i] = mean-t[t:ne(NaN)]:mean()
      end
      
      return bias
end


function computeSpareVector(train)
   
   local res = {}
   -- check whether this methods exists in torch
   for i = 1, train:size(1) do
      for j = 1, train:size(2) do
         local t = train[i][j] 

         if t ~= NaN then
            table.insert(res, 
               { 
                  i = i,
                  j = j,
                  val = t 
               })
         end 
      end
   end
   
   return res
end


function computeLearningRates(X, Y, loss)

   -- compute Hessian
   local Hessian = Y:t()*Y
   Hessian:mul(2)
   Hessian:div(Y:size(1))
   
   
   if loss then
      Hessian:add(loss:backward2())
   end
   
   
    local eigen = torch.symeig(Hessian)
    eigen:pow(-1)
    eigen:mul(2)
    
    local biasLearning  = torch.Tensor({eigen:min()})
    local learningRates = eigen:cat(biasLearning)

   return learningRates / 10000
end


--   -- Use enhanced Tanh form Lecun -> better fitting with -1/1 data : avoid saturating since second derivative is maximum for x = +/- 1
--   local ETanh = torch.class('nn.ETanh', 'nn.Module')
--   
--   function ETanh:updateOutput(input)
--      return input.nn.Tanh_updateOutput(self, input:mul(2/3)):mul(1.7159)
--   end
--   
--   function ETanh:updateGradInput(input, gradOutput)
--      return input.nn.Tanh_updateGradInput(self, input:mul(2/3), gradOutput):mul(2*1.7159/3)
--   end
--
--
--   -- Use enhanced Twisted : enhanced Tanh + linear term to avoid saturating
--   local TTanh = torch.class('nn.Tanh', 'nn.Module')
--   
--   function ETanh:updateOutput(input)
--      return input.nn.Tanh_updateOutput(self, input:mul(2/3)):mul(1.7159):add(0.1,input)
--   end
--   
--   function ETanh:updateGradInput(input, gradOutput)
--      return input.nn.Tanh_updateGradInput(self, input:mul(2/3), gradOutput):mul(2*1.7159/3):add(0.1)
--   end
   
