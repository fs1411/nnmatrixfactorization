require("nn")
require("optim")
require("torch")

dofile ("tools.lua")
dofile ("AlgoTools.lua")

dofile("regu.lua")


   
  
   
   
AlternateNN = {} 

function AlternateNN:new( conf )
  conf = conf or {}

  newObj = 
  {
     name = "ALS-NN",
     U = torch.Tensor(),
     V = torch.Tensor(),
     useSigmoid = conf.useSigmoid
  }
  
  if newObj.useSigmoid == true then
      newObj.name = newObj.name .. "-Sigmoid"
  end
  
  self.__index = self                      
  return setmetatable(newObj, self)        
end
   
function AlternateNN:init(U,V, train, conf)


   self.U = U:clone()
   self.V = V:clone()


   --store conf
   self.lambda = conf.lambda
   self.rank   = conf.rank
   
   

   self.lr  = conf.lrt
   self.lrU = conf.lrU
   self.lrV = conf.lrV
   
   self.train = train
   
   self.regu = reguL2(conf.lambda)
   self.loss = nn.MSECriterion()
   
   -- Construct ALS networks
   function appendNN(data, X)
   
      for i, x in pairs(data) do
      
         x.model = nn.Sequential()
         
         local layer = nn.Linear(self.rank, 1)
         layer.bias[1] = 0
         layer.weight:copy(X[i])
         
         x.model:add( layer )
         
         if self.useSigmoid == true then
             --x.model:add( nn.Sigmoid() )
             x.model:add( nn.Tanh() )
         end
      end
   end
   
   
   --precompute data
   self.nnU = computeSpareRates(train)
   appendNN(self.nnU, self.U) 
   self.Ubias = torch.Tensor(self.U:size(1))
      
   self.nnV = computeSpareRates(train:t())
   appendNN(self.nnV, self.V)
   self.Vbias = torch.Tensor(self.V:size(1))

end
   
   
function AlternateNN:eval(M, step)



      local function updateVector(U, networks, bias, sgdConf, V)
         
         -- preallocate memory
         local rating = torch.Tensor(1)
         
         for i, data in pairs(networks) do
         
            local noRatedElem = data.noRatedElem
         
            --retrieve data
            if noRatedElem > 0 then
               local mask = data.mask         
                     
               local r_ = data.rates
               local V_ = V[mask:expandAs(V)]:view(noRatedElem, self.rank)
               
               -- get the network to train
               local model = data.model
               model:training()
               
               -- Retrieve parameters and gradients
               local w, dw  = model:getParameters()
               
               -- check whether the output layer is indeed U
               --assert( model:get(1).weight:view(-1):eq(U[i]):byte():all() )

               
               -- shuffle data
               local shuffle = torch.randperm(noRatedElem)
               

               
               for ii = 1, shuffle:size(1) do
               
                  local shuffledIndex = shuffle[ii]

                  -- Define the closure to evalute l and dl
                  local function feval(u)
            
                     if w ~= u then
                        w:copy(u)
                     end
               
                     -- Reset gradients and losses (Compulsary, otherwise )
                     model:zeroGradParameters()
   
                     local v   = V_[shuffledIndex]
                     rating[1] = r_[shuffledIndex]
   
                     -- Forward propagation on the model
                     local y = model:forward(v)
                     
                     -- Forward/Backward propagation on the loss
                     local Err  = self.loss:forward(y, rating)
                     local dErr = self.loss:backward(y, rating)
                     
                     -- Backward propagation on the model -> accumulate gradient for backprop
                     local dy = model:backward(v, dErr)
   
                     -- Get the regularization loss
                     local regu     = self.regu:forward(u, noRatedElem)         
                     local gradRegu = self.regu:backward(u, noRatedElem)


                     -- Return loss and gradients
                     return (Err+regu), dw:add(gradRegu)
                  end
                  
                  -- Optimize current iteration
                  optim.sgd( feval, w, sgdConf )
               end
               
               
               -- update the input vector with the new network weigths
               U[i]:copy(model:get(1).weight)
               bias[i] = model:get(1).bias[1]
               
               -- stop training
               model:evaluate()
            end 

         end
      
      end
      
      -- Main algorithm
      updateVector(self.U, self.nnU, self.Ubias, {learningRate = self.lr, learningRates = self.lrU}, self.V)
      updateVector(self.V, self.nnV, self.Vbias, {learningRate = self.lr, learningRates = self.lrV}, self.U)
      
      M:copy(self.U*self.V:t())
      M:add(0.5, self.Ubias:view(-1, 1):expandAs(M))
      M:add(0.5, self.Vbias:view( 1,-1):expandAs(M))
      
      log("U*V' + B : " .. MSE(M, self.train))
      
      
      return M

end   
   
   


   
