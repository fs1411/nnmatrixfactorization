require("nn")
require("torch")

dofile ("tools.lua")













IniFactory = {}                                   

function IniFactory:new(data, confAlgo)                         
  newObj = 
  {
   U = torch.Tensor(data:size(1), confAlgo.rank):uniform(-0.1, 0.1),
   V = torch.Tensor(data:size(2), confAlgo.rank):uniform(-0.1, 0.1),
   rank = confAlgo.rank,
   data = data
  }  
  
  newObj.U[{{}, 1}] = 1
  newObj.V[{{}, 1}] = 1
 
  self.__index = self                      
  
  return setmetatable(newObj, self)        
end

function IniFactory:reset(newFeatures)

   if newFeatures then
      if newFeatures.rank then self.rank = newFeatures.rank end
      if newFeatures.data then self.data = newFeatures.data end
   end

   self.U  = torch.Tensor(self.data:size(1), self.rank):uniform(-0.01, 0.01)
   self.V  = torch.Tensor(self.data:size(2), self.rank):uniform(-0.01, 0.01)
   
   self.U[{{}, 1}] = 1
   self.V[{{}, 1}] = 1
end

function IniFactory:Build(config)

   log("Initialization : ")

   if config then
      if config.meanUser           then self:ApplyUserMean()         end
      if config.meanUserUnbiased   then self:ApplyUserUnbiasedMean() end
      if config.meanItem           then self:ApplyItemMean()         end
      if config.meanItemUnbiased   then self:ApplyItemUnbiasedMean() end
      if config.rootSquare         then self:ApplyRootSquare()       end
   end
   
   return self.U, self.V
end



-------------   MEAN    -----------------

local function ApplyMean(X, data)

   for i = 1, X:size(1) do

      local mask = data[i]:ne(NaN)
      
      if mask:sum() > 1 then
         local mean = data[i][mask]:mean()
         X[i][1] = mean
      end
   end
end

function IniFactory:ApplyUserMean()
   ApplyMean(self.U, self.data)
   log("use mean User")
end

function IniFactory:ApplyItemMean()
   ApplyMean(self.V, self.data:t())
   log("use mean Item")
end



-------------   UNBIASED MEAN    -----------------

--- Compute the mean of a movie by incorporating the global mean of the data
-- Source : http://sifter.org/~simon/journal/20061211.html
local function ApplyUnbiasedMean(X, data)

   local dataReduced = data[data:ne(NaN)]

   local gMean = dataReduced:mean()
   local gStd  = dataReduced:std()

   for i = 1, X:size(1) do

      local mask = data[i]:ne(NaN)
      
      
      if mask:sum() > 1 then
      
         local std  = data[i][mask]:std()
         local K = std/gStd
         
         X[i][1] = ( K*gMean +  data[i][mask]:sum()) / ( K + mask:sum() )
      end
   end
   
end


function IniFactory:ApplyUserUnbiasedMean()
   ApplyMean(self.U, self.data)
   log("use mean User unbiased")
end

function IniFactory:ApplyItemUnbiasedMean()
   ApplyMean(self.V, self.data:t())
   log("use mean Item unbiased")
end


function IniFactory:ApplyRootSquare()
   torch.sqrt(self.U[{{}, 1}])
   torch.sqrt(self.V[{{}, 1}])
   log("apply root Squares")
end



