local logging = require"logging"

local lastFileNameDatePattern
local lastFileHandler

local openFileLogger = function (filename, datePattern)
   local filename = string.format(filename, os.date(datePattern))
   if (lastFileNameDatePattern ~= filename) then
      local f = io.open(filename, "a")
      if (f) then
         f:setvbuf ("line")
         lastFileNameDatePattern = filename
         lastFileHandler = f
         return f
      else
         return nil, string.format("file `%s' could not be opened for writing", filename)
      end
   else
      return lastFileHandler
   end
end

function logging.dual(filename)
   if type(filename) ~= "string" then
      filename = "lualogging.log"
   end

   return logging.new( function(self, level, message)
      local f, msg = openFileLogger(filename, "")
      if not f then
         return nil, msg
      end
      f:write(message .. "\n")
      print(message)
      return true
   end)
end

return logging.dual


