require("nn")
require("torch")

dofile ("tools.lua")


--TODO : use POO 


local torchMSE  = nn.MSECriterion()
-- compute the mean squared error
function MSE(outputs, targets)


    local mask = targets:ne(NaN)

    local _outputs  = outputs[mask]
    local _targets = targets[mask]
    
--    local N = _targets:size(1)
--    local MSE = 1/N * torch.sum( torch.pow(_outputs - _targets, 2) )

    local loss = torchMSE:forward(_outputs, _targets)

    return round(math.sqrt(loss))

end


local torchBinaryCrossEntropy = nn.BCECriterion()
function BCE(outputs, targets)

    local mask = targets:ne(NaN)


    local _outputs = outputs[mask] 
    local _targets = targets[mask] 
    
    -- Scale values to compute binary cross entropy
    _outputs[_outputs:gt(1)] = 1-1e-12
    _outputs[_outputs:lt(0)] = 0+1e-12
    
    local loss = torchBinaryCrossEntropy:forward(_outputs, _targets)
    
    --local size = _outputs:size(1)
    --local C1   = _targets:clone():cmul(torch.log(_outputs))
    --local C2   = torch.ones(size):add(-1,_targets):cmul( torch.log( torch.ones(size):add(-1,_outputs) ))
    --local CE   = -torch.sum(C1 + C2) / size

    return round(loss)

end


function printLoss(M)

   log ("")
   log ("RMSE test  : " .. MSE(M, self.test))
   log ("RMSE train : " .. MSE(M, self.train))
   log ("")
   log ("CE test   : " .. BCE(M, self.test ))
   log ("CE train  : " .. BCE(M, self.train ))

end



lossStorage = {}
function lossStorage:new(epoches, train, test, conf)

  newObj = 
  {
   loss = 
   {
      MSE = 
      {
         functor = MSE,
         train   = torch.Tensor(epoches+1):zero(),
         test    = torch.Tensor(epoches+1):zero()
      },
      BCE = 
      {
         functor = BCE,
         train   = torch.Tensor(epoches+1):zero(),
         test    = torch.Tensor(epoches+1):zero()
      }
   },
   step = 0,
   train = postprocess(train:clone(), conf), -- return to raw data Assumption -> preProcess * postprocess = Identite)
   test  = postprocess(test:clone() , conf),
   conf  = conf
  }
  
  self.__index = self                      
  
  return setmetatable(newObj, self)        
end

function lossStorage:computeLoss(M, hasNaN)

   if hasNaN == nil then hasNaN = true end

   self.step =  self.step + 1

   postprocess(M, self.conf)

   for _, lossType in pairs(self.loss) do
      lossType.train[self.step] = lossType.functor(M, self.train)
      lossType.test[self.step]  = lossType.functor(M, self.test)
   end
  
end

function lossStorage:get(type, set, step)
   if step == nil then step = self.step end
   return self.loss[type][set][step]
end

function lossStorage:getBest(type, set)
   return self.loss[type][set][{{1,self.step}}]:min()
end
   
function lossStorage:build()
   return self.loss
end

   
