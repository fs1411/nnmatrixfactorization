require("nn")
require("torch")

dofile ("tools.lua")
dofile ("loss.lua")







Printer = {}


function Printer:new(displayConf, outputDir, dataType)
   
   --default behavior -> store results 
   if displayConf.store == nil then displayConf.store = true end
    
   newObj = 
      {
         figureIndex = 1,
         outputDir = outputDir,
         dataType = dataType,
         conf  = displayConf,
      }

   self.__index = self

   return setmetatable(newObj, self)        
end


-----------------
-- 
---  Tools
-- 

function Printer:plotOrStore(plot, filename)

   if self.conf.store == true then

      gnuplot.epsfigure(filename)
      gnuplot.figure(self.figureIndex)
      self.figureIndex = self.figureIndex + 1
      plot()
      gnuplot.plotflush()
   end

   if self.conf.plot == true then
      gnuplot.figure(self.figureIndex)
      self.figureIndex = self.figureIndex + 1
      plot()
   end
   
end




-----------------
-- 
---  Basic print
-- 

function Printer:loss(loss)

   log ("         MSE \t BCE \t")
   log ("test  : " .. loss:getBest("MSE", "test")  .. "\t" .. loss:getBest("BCE", "test"))
   log ("train : " .. loss:getBest("MSE", "train") .. "\t" .. loss:getBest("BCE", "train"))
   log("")

end

function Printer:algo(algo)

   
end


-----------------
-- 
---  Store files
-- 

function Printer:storeResults(M, name)

   if self.conf.store == true then

      local matrixFile = self.outputDir .. '/'.. self.dataType .. '-' .. name .. '.t7'
      local file = torch.DiskFile(matrixFile, 'w')
      if file then 
         file:writeObject(M)
         file:close() 
      end
   end
end


-----------------
-- 
---  plot Histogramme
-- 

function Printer:plotRatesDistribution(M, name, train, test)

   local plot = function()
   

      local appendix = ""
      if train and test then
      appendix = '  -  MSE: ' .. MSE(M, test) .. '/' .. MSE(M, train) ..
                 '  -  BCE: ' .. BCE(M, test) .. '/' .. MSE(M, train)  
      end 

      gnuplot.title(name .. appendix)
      gnuplot.xlabel('Rates')

      
      local mask = test:ne(NaN)
      
      local Y   = M[mask]
      local T   = postprocess(test[mask], self.conf)

      local mask2 = train:ne(NaN)
      
      local Y2   = M[mask2]
      local T2   = postprocess(train[mask2], self.conf)

      
      local hh = plotHist({ 
      {"Test distribution", T, "-"} , 
      {"Train distribution", T2, "-"},
      {"Estimate Distribution for Test sample", Y, "-"},
      {"Estimate Distribution for Train sample", Y2, "-"}
      })
      
      gnuplot.plot(hh)
      
--      self.figureIndex = self.figureIndex + 1
--      gnuplot.figure(self.figureIndex)
      
--      gnuplot.plot({
--         {"Test distribution error", hh[1][2],torch.abs(hh[1][3]-hh[3][3]), "~"} , 
--         {"Train distribution error",hh[1][2],torch.abs(hh[2][3]-hh[4][3]), "~"},
--         })
--      
      
   end
   
   local histoFile  = self.outputDir .. '/'.. self.dataType .. '-' .. name .. '.eps'

   self:plotOrStore(plot, histoFile)

end 


-----------------
-- 
---  Loss over time
-- 

local function LossToPlot(algoList, type, set)

   --Iterate over all the available algorithms to retrieve their loss over time
   local toPlot = {}
   for _, algo in pairs(algoList) do
      local loss = algo.loss[type][set]
      table.insert(toPlot, { algo.name, loss , '-'})
   end

   return toPlot
end


function Printer:plotLoss(algoList)

   -- Loook for all available errorType (MSE, BSE), Set (train/test) 
   for errStr, errType in pairs(algoList[1].loss) do
      for setStr, setType in pairs(errType) do

         if torch.isTensor(setType) then --focus on tensor -> avoid functor

            local toPlot = LossToPlot(algoList, errStr, setStr)

            local plot = function() 
               gnuplot.title(errStr .. "-" .. setStr)
               gnuplot.plot(toPlot)
            end
            
            local fileName  = self.outputDir .. '/'.. self.dataType .. '-' .. errStr .. '-' .. setStr .. '.eps'

            self:plotOrStore(plot, fileName)

         end
      end    
   end
end




