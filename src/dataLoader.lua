---------------------------------------


require("sys")
require("torch")

dofile ("tools.lua")



--- Load exernal file and turn into into a dense matrix:
--
-- @param config.name   (available : jester, movieLens)
-- @param config.rates  (required for : jester, movieLens)
-- @param config.users  (required for : movieLens)
-- @param config.movies (required for : movieLens)
--
function LoadData(config)

    log("")
    log("file (".. config.name ..") \t : " .. config.rates)
    log("")

    local data = torch.Tensor()

    local noUsers = 0
    local noMovies = 0
    local noRates = 0


    if config.name == "movieLens" then
       ---
       --  movieLens data are split into 3 files
       --   - users
       --   - movies
       --   - rates
       --
       --   Users/Movies files are first parsed to pre-allocate the rating matrix memory
       --

        -- step 1 : load users
        local usersfile = io.open(config.users, "r")
        for line in usersfile:lines() do
            --local userId, sex, age, job, ZIP = line:match('(%d+)::(%a)::(%d)::(%d)::(%d)')
            noUsers = noUsers + 1
        end
        usersfile:close()


        -- step 2 : load movies
        local moviesfile = io.open(config.movies, "r")
        
        local dumpedMovies = {} -- some movies were dumped therefore idMovie are not contiguous
        local expectedId = 1

        for line in moviesfile:lines() do
            local movieIdStr, title, genre = line:match('(%d+)::(.*)::(%a)')

            local movieId = tonumber(movieIdStr)

            -- store the Id of the movies that were dumped
            while expectedId < movieId do
                dumpedMovies[#dumpedMovies+1] = expectedId
                expectedId = expectedId + 1
            end

            expectedId = expectedId + 1
            noMovies = noMovies + 1
        end
        moviesfile:close()
        local greatestMovieId = expectedId-1
        assert( greatestMovieId - noMovies == #dumpedMovies)


        -- step 3 : load ratings
        local ratesfile = io.open(config.rates, "r")
        data = torch.Tensor(noUsers, greatestMovieId):fill(NaN)

        for line in ratesfile:lines() do
            local userIdStr, movieIdStr, rating, timestamp = line:match('(%d+)::(%d+)::(%d+)::(%d+)')

            local userId = tonumber(userIdStr)
            local movieId = tonumber(movieIdStr)

            data[userId][movieId] = rating

            noRates = noRates + 1
        end
        ratesfile:close()


        -- step 4 : remove the column of the movies that were dumped
        local mask = torch.ones(greatestMovieId):byte()
        for _, idMovie in pairs(dumpedMovies) do
            mask[idMovie] = 0
        end
        data = data[mask:view(1,-1):expandAs(data)]:view(noUsers,noMovies)




    elseif config.name == "jester" then
       ---
       --  Jester data are assumed to have torch formating
       --
    
        local file = torch.DiskFile(config.rates, "r")

        data = file:readObject()
        data[data:eq(99)] = NaN

        noUsers = data:size(1)
        noMovies = data:size(2)
        noRates = data:ne(99):sum()

    end

    log(noUsers .. " users were loaded.")
    log(noMovies .. " items were loaded.")
    log(noRates .. " rating were loaded.")

    return data

end
