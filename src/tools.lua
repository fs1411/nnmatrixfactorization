require("torch")

---------------------------------------
-- Basic Constant 

NaN = -999
Inf =  9999

math.randomseed(100)
torch.manualSeed(100)




Epsilon = function()
    return math.random() / 1000
end

function Sigmoid(x)
   if torch.isTensor(x) then
      return torch.pow(torch.exp(-x):add(1),-1)
   else
      return math.pow(math.exp(-x) + 1 ,-1)
   end
end


local buffer = torch.Tensor() 
function Logit(x)
   if torch.isTensor(x) then
      buffer:resizeAs(x):copy(x)
      buffer:add():pow(-1):cmul(x):add(1e-12):log()
      return buffer
   else

      return math.log( x / (1-x))
   end
end



noRollingFiles=3

function rollFile(name)

   -- remove the oldest file/directory
   os.execute('rm -rf ' .. name .. '.' .. noRollingFiles )
   
   -- rename the other file/directory
   for i = noRollingFiles-1, 1, -1 do
      local oldName = name .. '.' .. i 
      local newName = name .. '.' .. (i+1)
      os.rename(oldName,newName)
   end
   
   -- renome the newest file/directory to backup it
   os.rename(name, name .. '.1')
   
end

function round(num, idp)
   
  if idp == nil then idp = 4 end

  local mult = 10^(idp or 0)
  return math.floor(num * mult + 0.5) / mult
end



function merge(t1, t2)
    for k, v in pairs(t2) do
        if (type(v) == "table") and (type(t1[k] or false) == "table") then
            merge(t1[k], t2[k])
        else
            if t1[k] == nil then
               t1[k] = v
            end
        end
    end
    return t1
end


function plotHist(tensors, bins, min , max)

   hh = {}
   for _, tensor in pairs(tensors) do

      local h = gnuplot.histc(tensor[2],bins,min,max)
      local x_axis = torch.Tensor(#h)
      for i = 1,#h do
         x_axis[i] = h[i].val
      end
      
      hh[#hh+1] = {tensor[1], x_axis, h.raw:div(tensor[2]:size(1)), tensor[3]}

   end
   
   return hh

end





