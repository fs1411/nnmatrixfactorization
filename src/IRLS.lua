require("nn")
require("torch")

dofile ("tools.lua")
dofile ("AlgoTools.lua")

IRLS = {} 

function IRLS:new()
  newObj = 
  {
     name = "IRLS",
     U = torch.Tensor(),
     V = torch.Tensor(),
  }
  
  self.__index = self                      
  return setmetatable(newObj, self)        
end

function IRLS:init(U,V, train, conf)

   -- defensive copy
   self.U = U:clone()
   self.V = V:clone()

   --store conf
   self.lambda = conf.lambda
   self.rank   = conf.rank
   
   --precompute data
   self.sparseRates = {}

   self.sparseRates[torch.pointer(self.U)] = computeSpareRates(train)
   self.sparseRates[torch.pointer(self.V)] = computeSpareRates(train:t())
   
end



-- Avoid memory reallocation use copy/resize to greatly speed it up 

function IRLS:eval(M)


   local function updateVector(X, Y)
      
      -- TODO: pre-allocate memory for optim

      --retrieve the sparse data of the fix vector 
      local sparseRate = self.sparseRates[torch.pointer(X)]

      for i = 1, X:size(1) do

         local data = sparseRate[i]
         local _x   = X[i]
         
         local _noRatedElem = data.noRatedElem
         local _r           = data.rates
         local _mask        = data.mask:expandAs(Y)


         if _noRatedElem > 0 then

            -- Design matrix is the matrix Y reduced to the rated items by X
            local _Y = Y[_mask:expandAs(Y)]:view(_noRatedElem, self.rank)

            -- compute the target vector
            local _z = Sigmoid(_Y*_x)

            --compute the Weighting Matrix R
            local _Diag = _z:clone():cmul( torch.ones(_z:size(1))-_z ):diag()

            -- compute the regularization
            local _reguGrad = _x:clone():mul(2*self.lambda*_noRatedElem)
            local _reguHess = torch.eye(self.rank):mul(2*self.lambda*_noRatedElem)

            --compute the second order derivative of U
            local _grad = _Y:t() * (_z-_r) +_reguGrad
            local _Hess = _Y:t() * _Diag *_Y + _reguHess

            -- solve linear system : u' = u - Hess^-1 * Grad
            local dx = torch.gesv(-_grad:view(-1,1), _Hess) 
            _x:add(dx) 

         end
      end
   end


   -- Main algorithm
   updateVector(self.U, self.V)
   updateVector(self.V, self.U)

   -- Compute the estimate matrix
   M:copy(Sigmoid(self.U*self.V:t()))

   return M
   
end

