require("nn")
require("torch")

dofile ("tools.lua")



function executeAlgo(U, V, loss, train, test, epoches, conf)




   local     M = torch.Tensor():resizeAs(train):copy(U*V:t())
   local bestM = torch.Tensor():resizeAs(train):zero()

   local bestLoss = Inf
   

   if conf.lrt == "Hessian" then
      conf.lrt = 1
      conf.lrU = computeLearningRates(U, V)
      conf.lrV = computeLearningRates(V, U)
   end
   


   conf.algo:init(U, V, train, conf)
   
 
   -- execute
   for step = 1, epoches do

      --Display results
      if conf.displayAdvance == true then
         xlua.progress(step, epoches) 
      end

      -- apply algo (TODO : check no copy is done!)
      conf.algo:eval(M, step)

      -- store the results to plot it (false = no NaN in matrix)
      loss:computeLoss(M, false)


     
      -- look for the matrix that optimize the given loss
      local curloss  = loss:get(conf.lossToOptim, "test")

      if curloss < bestLoss then
         bestM:copy(M)
         bestLoss = curloss     
      end


      -- Conditional stop
      if conf.earlyStopping == true and bestLoss > curloss then
         log("Early stopping after " .. step .. " steps")
         break
      end

   end

   --post-processing
   return bestM, M

end






   

